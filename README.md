# API Platform

API Platform is a next-generation web framework designed by  [Les-Tilleuls.coop](https://les-tilleuls.coop/).

This repository is based on [API Platform](https://api-platform.com/) project.

## Install

Open a terminal, and navigate to the directory containing your project skeleton. Run the following command to start services using Docker Compose:

```bash
# Download the latest versions of the pre-built images
$ docker-compose pull

# Running in detached mode
$ docker-compose up -d
```

This command run just services _php_, _api_, _db_, _vulcain_ and _mercure_.  
To add UI _client_ and _admin_ you can use the following command.

```bash
$ docker-compose -f docker-compose.yml -f docker-compose-ui.yml pull
$ docker-compose -f docker-compose.yml -f docker-compose-ui.yml up -d
```

Project files are automatically shared between your local host machine and the container thanks to a pre-configured Docker volume. It means that you can edit files of your project locally using your preferred IDE or code editor, they will be transparently taken into account in the container.

The API Platform distribution comes with a dummy entity for test purpose: api/src/Entity/Greeting.php. You can try the API with a simple `GET` request

```
GET https://localhost:8443/greetings
```

## Services

| Name | Description | Port(s) | Environement(s) |
|------|-------------|---------|-----------------|
| php | The API with PHP, PHP-FPM 7.3, Composer and sensitive configs | n/a | all |
| api | The HTTP server for the API (NGINX) | n/a | all |
| db | A PostgreSQL database server | 5432 | all (prefer using a managed service in prod) |
| vulcain | The [Vulcain](https://github.com/dunglas/vulcain) gateway | 8443 | all |
| mercure | The [Mercure](https://api-platform.com/docs/core/mercure/) hub, for real-time capabilities | 1337 | all (prefer using the managed version in prod) |
| client | A development server for the Progressive Web App | 443 | dev (use a static website hosting service in prod) |
| admin | A development server for the admin | 444 | dev (use a static website hosting service in prod) |

